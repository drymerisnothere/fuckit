FROM nginx:alpine
RUN rm -rf /usr/share/nginx/html
COPY scripts/*.sh /usr/share/nginx/html/
COPY files/vhost.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
