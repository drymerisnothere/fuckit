# Fuckit

``` text
(╯°□°)╯︵ ┻━┻
```

Prank scripts to fuck around with coworkers. There's a bunch of scripts you can
choose, but if you don't care which to execute, then use the `shuffle.sh`
script, it will execute a random script. This scripts are accessible within
`fuckit.daemons.it`. Feel free to use it and to send more ideas or PR.

If you want to host this scripts, you should do a fork and change the domain
name from `shuffle.sh`.
