#!/usr/bin/env bash

# Add a time delay on every terminal it's launched

echo "sleep 0.5" | tee -a ~/.zshrc >> ~/.bashrc
