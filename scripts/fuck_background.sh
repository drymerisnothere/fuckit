#!/usr/bin/env bash

# Change the background

# Wallpaper
wget https://i.ytimg.com/vi/vvPfhb9SGAc/maxresdefault.jpg -O /tmp/hacked.jpg

# Gnome derivatives
gsettings get org.gnome.desktop.background picture-uri 'file:///tmp/hacked.jpg'

# KDE derivatives
dbus-send --session --dest=org.kde.plasmashell --type=method_call /PlasmaShell org.kde.PlasmaShell.evaluateScript 'string:
var Desktops = desktops();
for (i=0;i<Desktops.length;i++) {
        d = Desktops[i];
        d.wallpaperPlugin = "org.kde.image";
        d.currentConfigGroup = Array("Wallpaper",
                                    "org.kde.image",
                                    "General");
        d.writeConfig("Image", "file:///tmp/hacked.jpg");
}'

# XFCE
for i in $(xfconf-query -c xfce4-desktop -l | grep "last-image$"); do xfconf-query --channel xfce4-desktop --property $i --set /tmp/hacked.jpg; done

