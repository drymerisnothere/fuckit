#!/usr/bin/env bash

# Pretend to rm -rf /

fuck_remove() {
    directories=`ls --color=none /`
    for dir in $directories
    do
        for file in `ls --color=none /$dir`
        do
            echo removed \'/$dir/$file\'
            sleep 0.05
        done
    done
}

if [[ "$SHELL" == *"zsh"* ]]
then
    source_function="$(which fuck_remove)"
else
    # Assume bash
    source_function="$(type fuck_remove | grep -v 'is a function')"
fi

echo "$source_function" | tee -a ~/.zshrc >> ~/.bashrc
echo "fuck_remove" | tee -a ~/.zshrc >> ~/.bashrc
